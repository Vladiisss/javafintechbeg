package HW2;

import java.util.ArrayList;

public class TestClass2 {
    public static void main(String[] args) {

        int n = 4;
        ArrayList<Coordinates> vertexes = new ArrayList<>(n);

        for (int i = 0; i < n; i++) {
            vertexes.add(new Coordinates());
        }
        // {{1.0, 1.0},{4.0, 1.0},{4.0, 4.0},{1.0, 4.0}};
        vertexes.get(0).x = 1.0;
        vertexes.get(0).y = 1.0;
        vertexes.get(1).x = 4.0;
        vertexes.get(1).y = 1.0;
        vertexes.get(2).x = 4.0;
        vertexes.get(2).y = 4.0;
        vertexes.get(3).x = 1.0;
        vertexes.get(3).y = 4.0;

        ArrayList<Coordinates> vertexes2 = new ArrayList<>(vertexes); // Используем конструктор копий

        Polygon polygon = new Polygon(n, vertexes);
        System.out.println(polygon.getPerimeter());
        System.out.println(polygon.getArea());

        Rhomb rhomb = new Rhomb(vertexes2);
        System.out.println(rhomb.getPerimeter());
        System.out.println(rhomb.getArea());
        System.out.println(rhomb.getSide());

    }
}

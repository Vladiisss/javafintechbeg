package HW2;

/**
 * Интерфейс для геометрических фигур с ребрами
 */
public interface WithRibs {

    // Возвращает сторону фигуры
    public double getSide();
}

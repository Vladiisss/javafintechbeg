package HW2;


public class TestClass1 {

    public static void main(String[] args) {

        Circle circle = new Circle(10, "Black");
        Rectangle rectangle = new Rectangle(2, 4);

        System.out.println(circle.getPerimeter());
        System.out.println(circle.getArea());
        System.out.println(circle);
        circle.colorCircle("Red");
        System.out.println(circle);

        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getDiagonal());
    }
}

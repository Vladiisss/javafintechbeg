package HW2;


public class Rectangle extends GeomFigure {

    private double a;
    private double b;

    public Rectangle() {}

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return (a + b) * 2;
    }

    @Override
    public double getArea() {
        return a * b;
    }

    public double getDiagonal() {
        return Math.sqrt(a * a + b * b);
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}

package HW2;

import java.util.ArrayList;

public class Rhomb extends Polygon implements WithRibs {



    public Rhomb(ArrayList<Coordinates> newVertexes) {
        super(4, newVertexes);
        numberVertices = 4;
    }

    @Override
    public double getSide() {
        return getPerimeter() / 4.0;
    }
}

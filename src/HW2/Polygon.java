package HW2;

import java.util.ArrayList;

public class Polygon extends GeomFigure implements WithAngles {

    protected int numberVertices; // Кол-во вершин
    protected ArrayList<Coordinates> vertexes = new ArrayList<>(10); // Массив координат вершин


    public Polygon(int numberVertices, ArrayList<Coordinates> vertexes) {
        this.numberVertices = numberVertices;
        this.vertexes = vertexes;
    }

    // Возвращает результат формулы d = sqrt((x2 - x1)^2 + (y2 - y1)^2)
    // Формула находит расстояние между двумя вершинами
    private double distance(double x2, double x1, double y2, double y1) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    @Override
    public double getPerimeter() {
        double d, perim = 0.0;
        Coordinates vertex2;

        for (int i = 0; i < numberVertices; i++) {

            if (i == numberVertices - 1) {
                vertex2 = vertexes.get(0);
            } else {
                vertex2 = vertexes.get(i + 1);
            }

            d = distance(vertexes.get(i).x, vertex2.x, vertexes.get(i).y, vertex2.y);
            perim += d;
        }
        return perim;
    }

    // S = 1/2 * |(x1*y2 + x2*y3 + ... + xn*y1) - (y1*x2 + y2*x3 + ... + yn*x1)|
    @Override
    public double getArea() {
        double sm1 = 0.0, sm2 = 0.0;
        Coordinates vertex2;

        for (int i = 0; i < numberVertices; i++) {

            if (i == numberVertices - 1) {
                vertex2 = vertexes.get(0);
            } else {
                vertex2 = vertexes.get(i + 1);
            }

            sm1 += vertexes.get(i).x * vertex2.y;
            sm2 += vertexes.get(i).y * vertex2.x;
        }
        return Math.abs(sm1 - sm2) / 2.0;
    }

    @Override
    public void printVertexes() {
        System.out.println(vertexes);
    }
}

